extern crate temperature_converter;
use std::io;

fn main() {
    loop {
        println!("Please enter temperature input with format (value, unit) -> (10, C)");
        println!("units are: ");
        for u in temperature_converter::temperature_converter::TemperatureUnits::iterator() {
            match u {
                &temperature_converter::temperature_converter::TemperatureUnits::C => println!("{} - Celsius", u),
                &temperature_converter::temperature_converter::TemperatureUnits::F => println!("{} - Fahrenheit", u),
                &temperature_converter::temperature_converter::TemperatureUnits::K => println!("{} - Kelvin", u),
            }
        }

        let mut input_temperature = String::new();

        match io::stdin().read_line(&mut input_temperature) {
            Ok(line) => line,
            Err(_) => {
                println!("failed to read line, please try again");
                println!("");
                continue;
            }
        };
        let comma_index = input_temperature.find(",");

        match comma_index {
            None => {
                println!("no comma found");
                continue;
            }
            Some(index) => {
                let value = &input_temperature[..index];
                let value: f32 = match value.trim().parse() {
                    Ok(num) => num,
                    Err(_) => {
                        println!("Invalid value, please enter a number");
                        println!("");
                        continue;
                    }
                };

                let unit = &input_temperature[index + 1..];
                let unit: temperature_converter::temperature_converter::TemperatureUnits = {
                    let u = unit.trim();
                    match u {
                        "C" => temperature_converter::temperature_converter::TemperatureUnits::C,
                        "F" => temperature_converter::temperature_converter::TemperatureUnits::F,
                        "K" => temperature_converter::temperature_converter::TemperatureUnits::K,
                        _ => {
                            println!("Invalid unit {}, please provide C, F or K (Celsius, Fahrenheit, Kelvin)", u);
                            println!("");
                            continue;
                        }
                    }
                };

                let mut to: String = String::new();

                println!("To convert to ?");

                match io::stdin().read_line(&mut to) {
                    Ok(line) => line,
                    Err(_) => {
                        println!("failed to read line, please try again");
                        println!("");
                        continue;
                    }
                };

                let to: temperature_converter::temperature_converter::TemperatureUnits = {
                    let t = to.trim();
                    match t {
                        "C" => temperature_converter::temperature_converter::TemperatureUnits::C,
                        "F" => temperature_converter::temperature_converter::TemperatureUnits::F,
                        "K" => temperature_converter::temperature_converter::TemperatureUnits::K,
                        _ => {
                            println!("Invalid transformation {}, please provide C, F or K (Celsius, Fahrenheit, Kelvin)", t);
                            println!("");
                            continue;
                        }
                    }
                };

                let old_temperature: (f32, temperature_converter::temperature_converter::TemperatureUnits) = (value, unit);
                let temperature = temperature_converter::temperature_converter::convert_temperature(old_temperature, to);
                println!(
                    "temperature: {} {} = {} {}",
                    old_temperature.0, old_temperature.1, temperature.0, temperature.1
                );
                break;
            }
        }
    }
}
