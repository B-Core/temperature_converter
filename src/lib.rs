pub mod temperature_converter {
    use std::fmt;
    use std::slice::Iter;

    // derive copy and clone to respect ownership
    #[derive(Clone, Copy, Debug)]
    pub enum TemperatureUnits {
        C, // Celsius
        F, // Fahrenheit
        K, // Kelvin
    }

    // implement iterator function for the enum
    impl TemperatureUnits {
        pub fn iterator() -> Iter<'static, TemperatureUnits> {
            static T_UNITS: [TemperatureUnits; 3] = [
                TemperatureUnits::C,
                TemperatureUnits::F,
                TemperatureUnits::K,
            ];
            T_UNITS.into_iter()
        }
    }

    // implement Display function to be able to display in println
    // the value of the enum
    impl fmt::Display for TemperatureUnits {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            match *self {
                TemperatureUnits::F => write!(f, "F"),
                TemperatureUnits::C => write!(f, "C"),
                TemperatureUnits::K => write!(f, "K"),
            }
        }
    }

    fn convert_to_celsius(temperature: (f32, TemperatureUnits)) -> (f32, TemperatureUnits) {
        let value: f32 = match temperature.1 {
            TemperatureUnits::C => temperature.0,
            TemperatureUnits::F => (temperature.0 - 32.0) / 1.8,
            TemperatureUnits::K => temperature.0 - 273.149994,
        };
        let unit: TemperatureUnits = temperature.1;
        (value, unit)
    }

    pub fn convert_temperature(
        temperature: (f32, TemperatureUnits),
        to: TemperatureUnits,
    ) -> (f32, TemperatureUnits) {
        let temperature = convert_to_celsius(temperature);
        let value: f32 = {
            match to {
                TemperatureUnits::C => temperature.0,
                TemperatureUnits::F => (temperature.0 * 1.8) + 32.0,
                TemperatureUnits::K => temperature.0 + 273.15,
            }
        };
        let unit: TemperatureUnits = to;

        (value, unit)
    }
}
