extern crate temperature_converter;

#[test]
fn it_converts_celsius_to_fahrenheit() {
  let inputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (0.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (10.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (15.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (20.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (22.3, temperature_converter::temperature_converter::TemperatureUnits::C),
    (30.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (40.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (42.574, temperature_converter::temperature_converter::TemperatureUnits::C),
    (50.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (60.0, temperature_converter::temperature_converter::TemperatureUnits::C),
  ];
  let outputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (32.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (50.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (59.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (68.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (72.14, temperature_converter::temperature_converter::TemperatureUnits::F),
    (86.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (104.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (108.6332, temperature_converter::temperature_converter::TemperatureUnits::F),
    (122.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (140.0, temperature_converter::temperature_converter::TemperatureUnits::F),
  ];
  
  let mut index = inputs.len() - 1;
  while index != 0 {
    let converted = temperature_converter::temperature_converter::convert_temperature(inputs[index], temperature_converter::temperature_converter::TemperatureUnits::F);
    assert_eq!(outputs[index].0, converted.0);
    assert_eq!(outputs[index].1.to_string(), converted.1.to_string());
    index -= 1;
  }
}

#[test]
fn it_converts_celsius_to_kelvin() {
  let inputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (0.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (10.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (15.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (20.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (22.3, temperature_converter::temperature_converter::TemperatureUnits::C),
    (30.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (40.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (42.574, temperature_converter::temperature_converter::TemperatureUnits::C),
    (50.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (60.0, temperature_converter::temperature_converter::TemperatureUnits::C),
  ];
  let outputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (273.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (283.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (288.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (293.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (295.44998, temperature_converter::temperature_converter::TemperatureUnits::K),
    (303.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (313.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (315.724, temperature_converter::temperature_converter::TemperatureUnits::K),
    (323.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (333.15, temperature_converter::temperature_converter::TemperatureUnits::K),
  ];
  
  let mut index = inputs.len() - 1;
  while index != 0 {
    let converted = temperature_converter::temperature_converter::convert_temperature(inputs[index], temperature_converter::temperature_converter::TemperatureUnits::K);
    assert_eq!(outputs[index].0, converted.0);
    assert_eq!(outputs[index].1.to_string(), converted.1.to_string());
    index -= 1;
  }
}

#[test]
fn it_converts_celsius_to_celsius() {
  let inputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (0.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (10.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (15.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (20.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (22.3, temperature_converter::temperature_converter::TemperatureUnits::C),
    (30.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (40.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (42.574, temperature_converter::temperature_converter::TemperatureUnits::C),
    (50.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (60.0, temperature_converter::temperature_converter::TemperatureUnits::C),
  ];
  let outputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (0.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (10.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (15.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (20.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (22.3, temperature_converter::temperature_converter::TemperatureUnits::C),
    (30.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (40.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (42.574, temperature_converter::temperature_converter::TemperatureUnits::C),
    (50.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (60.0, temperature_converter::temperature_converter::TemperatureUnits::C),
  ];
  
  let mut index = inputs.len() - 1;
  while index != 0 {
    let converted = temperature_converter::temperature_converter::convert_temperature(inputs[index], temperature_converter::temperature_converter::TemperatureUnits::C);
    assert_eq!(outputs[index].0, converted.0);
    assert_eq!(outputs[index].1.to_string(), converted.1.to_string());
    index -= 1;
  }
}

#[test]
fn it_converts_fahrenheit_to_celsius() {
  let inputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (32.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (50.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (59.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (68.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (72.14, temperature_converter::temperature_converter::TemperatureUnits::F),
    (86.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (104.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (108.6332, temperature_converter::temperature_converter::TemperatureUnits::F),
    (122.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (140.0, temperature_converter::temperature_converter::TemperatureUnits::F),
  ];
  let outputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (0.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (10.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (15.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (20.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (22.300001, temperature_converter::temperature_converter::TemperatureUnits::C),
    (30.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (40.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (42.574, temperature_converter::temperature_converter::TemperatureUnits::C),
    (50.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (60.0, temperature_converter::temperature_converter::TemperatureUnits::C),
  ];
  
  let mut index = inputs.len() - 1;
  while index != 0 {
    let converted = temperature_converter::temperature_converter::convert_temperature(inputs[index], temperature_converter::temperature_converter::TemperatureUnits::C);
    assert_eq!(outputs[index].0, converted.0);
    assert_eq!(outputs[index].1.to_string(), converted.1.to_string());
    index -= 1;
  }
}

#[test]
fn it_converts_fahrenheit_to_kelvin() {
  let inputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (32.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (50.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (59.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (68.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (72.14, temperature_converter::temperature_converter::TemperatureUnits::F),
    (86.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (104.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (108.6332, temperature_converter::temperature_converter::TemperatureUnits::F),
    (122.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (140.0, temperature_converter::temperature_converter::TemperatureUnits::F),
  ];
  let outputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (273.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (283.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (288.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (293.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (295.44998, temperature_converter::temperature_converter::TemperatureUnits::K),
    (303.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (313.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (315.724, temperature_converter::temperature_converter::TemperatureUnits::K),
    (323.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (333.15, temperature_converter::temperature_converter::TemperatureUnits::K),
  ];
  
  let mut index = inputs.len() - 1;
  while index != 0 {
    let converted = temperature_converter::temperature_converter::convert_temperature(inputs[index], temperature_converter::temperature_converter::TemperatureUnits::K);
    assert_eq!(outputs[index].0, converted.0);
    assert_eq!(outputs[index].1.to_string(), converted.1.to_string());
    index -= 1;
  }
}

#[test]
fn it_converts_fahrenheit_to_fahrenheit() {
  let inputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (32.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (50.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (59.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (68.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (72.14, temperature_converter::temperature_converter::TemperatureUnits::F),
    (86.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (104.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (108.6332, temperature_converter::temperature_converter::TemperatureUnits::F),
    (122.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (140.0, temperature_converter::temperature_converter::TemperatureUnits::F),
  ];
  let outputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (32.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (50.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (59.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (68.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (72.14, temperature_converter::temperature_converter::TemperatureUnits::F),
    (86.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (104.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (108.6332, temperature_converter::temperature_converter::TemperatureUnits::F),
    (122.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (140.0, temperature_converter::temperature_converter::TemperatureUnits::F),
  ];
  
  let mut index = inputs.len() - 1;
  while index != 0 {
    let converted = temperature_converter::temperature_converter::convert_temperature(inputs[index], temperature_converter::temperature_converter::TemperatureUnits::F);
    assert_eq!(outputs[index].0, converted.0);
    assert_eq!(outputs[index].1.to_string(), converted.1.to_string());
    index -= 1;
  }
}

#[test]
fn it_converts_kelvin_to_celsius() {
  let inputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (273.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (283.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (288.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (293.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (295.44998, temperature_converter::temperature_converter::TemperatureUnits::K),
    (303.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (313.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (315.724, temperature_converter::temperature_converter::TemperatureUnits::K),
    (323.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (333.15, temperature_converter::temperature_converter::TemperatureUnits::K),
  ];
  let outputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (0.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (10.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (15.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (20.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (22.299988, temperature_converter::temperature_converter::TemperatureUnits::C),
    (30.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (40.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (42.574005, temperature_converter::temperature_converter::TemperatureUnits::C),
    (50.0, temperature_converter::temperature_converter::TemperatureUnits::C),
    (60.0, temperature_converter::temperature_converter::TemperatureUnits::C),
  ];
  
  let mut index = inputs.len() - 1;
  while index != 0 {
    let converted = temperature_converter::temperature_converter::convert_temperature(inputs[index], temperature_converter::temperature_converter::TemperatureUnits::C);
    assert_eq!(outputs[index].0, converted.0);
    assert_eq!(outputs[index].1.to_string(), converted.1.to_string());
    index -= 1;
  }
}

#[test]
fn it_converts_kelvin_to_fahrenheit() {
  let inputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (273.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (283.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (288.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (293.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (295.44998, temperature_converter::temperature_converter::TemperatureUnits::K),
    (303.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (313.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (315.724, temperature_converter::temperature_converter::TemperatureUnits::K),
    (323.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (333.15, temperature_converter::temperature_converter::TemperatureUnits::K),
  ];
  let outputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (32.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (50.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (59.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (68.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (72.13998, temperature_converter::temperature_converter::TemperatureUnits::F),
    (86.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (104.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (108.63321, temperature_converter::temperature_converter::TemperatureUnits::F),
    (122.0, temperature_converter::temperature_converter::TemperatureUnits::F),
    (140.0, temperature_converter::temperature_converter::TemperatureUnits::F),
  ];
  
  let mut index = inputs.len() - 1;
  while index != 0 {
    let converted = temperature_converter::temperature_converter::convert_temperature(inputs[index], temperature_converter::temperature_converter::TemperatureUnits::F);
    assert_eq!(outputs[index].0, converted.0);
    assert_eq!(outputs[index].1.to_string(), converted.1.to_string());
    index -= 1;
  }
}

#[test]
fn it_converts_kelvin_to_kelvin() {
  let inputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (273.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (283.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (288.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (293.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (295.44998, temperature_converter::temperature_converter::TemperatureUnits::K),
    (303.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (313.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (315.724, temperature_converter::temperature_converter::TemperatureUnits::K),
    (323.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (333.15, temperature_converter::temperature_converter::TemperatureUnits::K),
  ];
  let outputs: [(f32, temperature_converter::temperature_converter::TemperatureUnits); 10] = [
    (273.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (283.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (288.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (293.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (295.44998, temperature_converter::temperature_converter::TemperatureUnits::K),
    (303.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (313.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (315.724, temperature_converter::temperature_converter::TemperatureUnits::K),
    (323.15, temperature_converter::temperature_converter::TemperatureUnits::K),
    (333.15, temperature_converter::temperature_converter::TemperatureUnits::K),
  ];
  
  let mut index = inputs.len() - 1;
  while index != 0 {
    let converted = temperature_converter::temperature_converter::convert_temperature(inputs[index], temperature_converter::temperature_converter::TemperatureUnits::K);
    assert_eq!(outputs[index].0, converted.0);
    assert_eq!(outputs[index].1.to_string(), converted.1.to_string());
    index -= 1;
  }
}